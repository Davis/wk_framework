dnl $Id$
dnl config.m4 for extension wk_framework

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

PHP_ARG_WITH(wk_framework, for wk_framework support,
dnl Make sure that the comment is aligned:
[  --with-wk_framework             Include wk_framework support])

dnl Otherwise use enable:

PHP_ARG_ENABLE(wk_framework, whether to enable wk_framework support,
dnl Make sure that the comment is aligned:
[  --enable-wk_framework           Enable wk_framework support])

if test "$PHP_WK_FRAMEWORK" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-wk_framework -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/wk_framework.h"  # you most likely want to change this
  dnl if test -r $PHP_WK_FRAMEWORK/$SEARCH_FOR; then # path given as parameter
  dnl   WK_FRAMEWORK_DIR=$PHP_WK_FRAMEWORK
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for wk_framework files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       WK_FRAMEWORK_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$WK_FRAMEWORK_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the wk_framework distribution])
  dnl fi

  dnl # --with-wk_framework -> add include path
  dnl PHP_ADD_INCLUDE($WK_FRAMEWORK_DIR/include)

  dnl # --with-wk_framework -> check for lib and symbol presence
  dnl LIBNAME=wk_framework # you may want to change this
  dnl LIBSYMBOL=wk_framework # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $WK_FRAMEWORK_DIR/lib, WK_FRAMEWORK_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_WK_FRAMEWORKLIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong wk_framework lib version or lib not found])
  dnl ],[
  dnl   -L$WK_FRAMEWORK_DIR/lib -lm
  dnl ])
  dnl
  dnl PHP_SUBST(WK_FRAMEWORK_SHARED_LIBADD)

  PHP_NEW_EXTENSION(wk_framework, wk_framework.c wk_config.c wk_controller.c wk_input.c wk_load.c wk_model.c wk_view.c, $ext_shared)
fi
