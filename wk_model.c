/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2013 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/
 
/* $Id$ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "php_wk_framework.h"
#include "wk_model.h"

zend_class_entry *wk_model_ce;
static int le_wk_model;

ZEND_BEGIN_ARG_INFO_EX(void_arginfo, 0, 0, 0)
ZEND_END_ARG_INFO()

static zend_function_entry wk_load_method[] = {
	{NULL, NULL, NULL}
};

EXT_STARTUP_FUNCTION(WK_Model){
	zend_class_entry wk_model;
	INIT_CLASS_ENTRY(wk_model, "WK_Model", wk_load_method);
	wk_model_ce = zend_register_internal_class(&wk_model TSRMLS_CC);
	return SUCCESS;
}