<!doctype html>
<html>
<head>
<title><?php echo SITE_NAME;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo ID_SERVER;?>static/css/base.css" rel="stylesheet" type="text/css" />
</head>
<body>

<!-- 系统提示 -->
<div class="register">
<!-- 主区域 -->
<div class="main prompt">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td height="160">
<img class="system" src="<?php echo ID_SERVER;?>static/images/system/correct.png" />
<span><?php echo htmlspecialchars($msg, ENT_COMPAT);?><br />运行成功 <b id="num"><?php echo htmlspecialchars($seconds, ENT_COMPAT);?></b></span>
</td>
</tr>
</table>
</div>
</div>

<script type="text/JavaScript">
var secs = <?php echo htmlspecialchars($seconds, ENT_COMPAT);?>; //倒计时的秒数 
for(var i = secs; i >= 0; i --){
window.setTimeout('doUpdate(' + i + ')', (secs - i) * 1000);
} 

function doUpdate(num){
document.getElementById('num').innerHTML = num;
var win = window.parent ? window.parent : window;
<?php if($url) { ?>
if (num == 0) win.location = '<?php echo htmlspecialchars($url, ENT_COMPAT);?>';
<?php } else { ?>
if (num == 0) win.history.go(-1);
<?php } ?>
}
</script>

</body>
</html>