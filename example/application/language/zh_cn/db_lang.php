<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 数据库
 *
 * @version Id:  db_lang.php 下午4:53:29 2012-11-16   $
 */

$lang['db_invalid_connection_str'] = '无法确定根据您所提交的数据库设置';
$lang['db_unable_to_connect'] = '根据您的配置，无法连接到数据库服务器';
$lang['db_unable_to_select'] = '无法选择指定的数据库: %s';
$lang['db_unable_to_create'] = '无法创建指定的数据库: %s';
$lang['db_invalid_query'] = '您提交的查询是无效的';
$lang['db_must_set_table'] = '您必须设置查询中使用的数据表';
$lang['db_must_use_set'] = '您必须使用“set”来更新一条记录';
$lang['db_must_use_index'] = '您必须指定一个索引来匹配批量更新';
$lang['db_batch_missing_index'] = '提交批次更新的一个或多个行缺少指定索引';
$lang['db_must_use_where'] = '不允许更新，除非它们包含一个“where”子句';
$lang['db_del_must_use_where'] = '不允许删除，除非它们包含一个“where”或者“like”子句';
$lang['db_field_param_missing'] = '获取字段需要的表名作为参数';
$lang['db_unsupported_function'] = '您使用的数据库不允许使用这个功能';
$lang['db_transaction_failure'] = '交易失败：执行回滚';
$lang['db_unable_to_drop'] = '无法删除指定的数据库';
$lang['db_unsuported_feature'] = '您使用的数据库平台不支持此功能';
$lang['db_unsuported_compression'] = '您选择的文件压缩格式不支持你的服务器';
$lang['db_filepath_error'] = '无法写入数据到您所提交的文件路径';
$lang['db_invalid_cache_path'] = '您所提交的缓存路径是无效或不可写';
$lang['db_table_name_required'] = '该操作需要指定一个表名';
$lang['db_column_name_required'] = '该操作需要指定一个字段名';
$lang['db_column_definition_required'] = '该操作需要定义一个字段';
$lang['db_unable_to_set_charset'] = '无法设置客户端连接的字符集: %s';
$lang['db_error_heading'] = '出现数据库错误';


/* End of file db_lang.php */
/* Location: ./application/language/zh_cn/db_lang.php */