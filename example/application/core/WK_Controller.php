<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends WK_Controller{
	public function __construct(){
		parent::__construct();
		define('CONTROLLER', $this->controller);
		define('MODULE', $this->module);
		define('SITE_NAME', '测试用例');
	}
}