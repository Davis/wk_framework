<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 扩展CI的字符串辅助函数
 *
 * @version Id:  string_helper.php 下午4:51:58 2012-11-16   $
 */


if ( ! function_exists('string_exists'))
{
    /**
     * 判断字符串中是否存在某个字符
     *
     * @param string  $string 字符串
     * @param string  $find 某个字符
     * @return boolean
     */
    function string_exists($string, $find)
    {
        return ! (strpos($string, $find) === FALSE);
    }
}

if ( ! function_exists('dstrlen'))
{
    /**
     * 获取字符串长度
     * 
     * @param string  $string 字符串
     * @param string  $find 某个字符
     * @return boolean
     */
    function dstrlen($str, $type = 'tencent')
    {
        $count = 0;
        for ($i = 0; $i < strlen($str); $i++) 
        {
            $value = ord($str[$i]);
            if ($value > 127) 
            {
                $count++;
                if ($value >= 192 && $value <= 223) 
                {
                    $i++;
                } 
                elseif ($value >= 224 && $value <= 239) 
                {
                    $i = $i + 2;
                } 
                elseif ($value >= 240 && $value <= 247) 
                {
                    $i = $i + 3;
                }
            }
            $count++;
        }
        if ($type == 'sina') 
        {
            $min = 20;
            $sina = '/http:\/\/[a-zA-Z0-9]+(\.[a-zA-Z0-9]+)+([-A-Z0-9a-z_\$\.\+\!\*\(\)\/,:;@&=\?\~\#\%]*)*/';
            preg_match_all($sina, $str, $array);
        } 
        else 
        {
            $min = 22;
            $tencent = '/((news|telnet|nttp|file|http|ftp|https):\/\/){1}(([-A-Za-z0-9]+(\\.[-A-Za-z0-9]+)*(\\.[-A-Za-z]{2,5}))|([0-9]{1,3}(\\.[0-9]{1,3}){3}))(:[0-9]*)?(\/[-A-Za-z0-9_\\$\\.\\+\\!\\*\\(\\),;:@&=\\?\/~\\#\\%]*)*/';
            preg_match_all($tencent, $str, $array);
        }
        $len = 0;
        if (is_array($array) && !empty($array)) 
        {
            foreach ($array[0] as $v) 
            {
                if (strlen($v) > $min || $type == 'tencent') 
                {
                    $len += strlen($v) - $min;
                }
            }
        }
        return $count - $len;
    }
}


if ( ! function_exists('cut_string'))
{
    /**
     * 截取指定长度的字符串
     *
     * @param string  $string 字符串
     * @param integer $length 截取长度
     * @param string  $dot 后缀字符
     * @return string
     */
    function cut_string($string, $length, $dot = '...')
    {
        if (strlen($string) <= $length)
        {
            return $string;
        }

        $pre = chr(1);
        $end = chr(1);
        $string = str_replace(array('&amp;', '&quot;', '&lt;', '&gt;'), array($pre . '&' . $end, $pre . '"' . $end, $pre . '<' . $end, $pre . '>' . $end), $string);

        $n = $tn = $noc = 0;
        while ($n < strlen($string))
        {
            $t = ord($string[$n]);
            if ($t == 9 || $t == 10 || (32 <= $t && $t <= 126))
            {
                $tn = 1; $n++; $noc++;
            }
            elseif (194 <= $t && $t <= 223)
            {
                $tn = 2; $n += 2; $noc += 2;
            }
            elseif (224 <= $t && $t <= 239)
            {
                $tn = 3; $n += 3; $noc += 2;
            }
            elseif (240 <= $t && $t <= 247)
            {
                $tn = 4; $n += 4; $noc += 2;
            }
            elseif (248 <= $t && $t <= 251)
            {
                $tn = 5; $n += 5; $noc += 2;
            }
            elseif ($t == 252 || $t == 253)
            {
                $tn = 6; $n += 6; $noc += 2;
            }
            else
            {
                $n++;
            }

            if ($noc >= $length)
            {
                break;
            }
        }

        if ($noc > $length)
        {
            $n -= $tn;
        }

        $strcut = substr($string, 0, $n);
        $strcut = str_replace(array($pre . '&' . $end, $pre . '"' . $end, $pre . '<' . $end, $pre . '>' . $end), array('&amp;', '&quot;', '&lt;', '&gt;'), $strcut);

        $pos = strrpos($strcut, chr(1));
        if ($pos !== false)
        {
            $strcut = substr($strcut, 0, $pos);
        }

        return $strcut . $dot;
    }
}