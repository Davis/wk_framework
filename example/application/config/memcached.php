<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


// --------------------------------------------------------------------------
// Servers
// --------------------------------------------------------------------------
$config['memcached'] = array(

	   '1' => array(
			'host'			=> '127.0.0.1',
			'port'			=> '12000',
			'weight'		=> '1',
	        'prefix'        => 'wk_memcache_1.0',
			'persistent'	=> FALSE, 
	        'expiration'	=> 0,
	        'compression'   => FALSE,
	        'delete_expiration'	 => 0				
		)
);

/* End of file memcached.php */
/* Location: ./system/application/config/memcached.php */