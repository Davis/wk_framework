/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2013 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h" 
#include "php_ini.h"
#include "ext/standard/info.h"
#include "main/SAPI.h"  
#include "Zend/zend_interfaces.h"  
#include "ext/standard/php_var.h"  
#include "ext/standard/php_string.h"  
#include "ext/standard/php_smart_str.h"  
#include "ext/standard/url.h"
#include "ext/standard/php_string.h"
#include "php_wk_framework.h"
#include "wk_model.h"
#include "wk_load.h"
#include "wk_controller.h"
#include "wk_input.h"
#include "wk_config.h"

zend_class_entry *wk_config_ce;
static int le_wk_config;

ZEND_BEGIN_ARG_INFO_EX(void_arginfo, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(item_arginfo, 0, 0, 1)
ZEND_ARG_INFO(0, item_name)
ZEND_ARG_INFO(0, item_value)
ZEND_END_ARG_INFO()

PHP_METHOD(wk_config, __construct){
}

PHP_METHOD(wk_config, item){
	char *name, *val;
	int name_len, val_len;
	zval *config;
	zval **ret;

	if( zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "s|s", &name, &name_len) == FAILURE ){
		RETURN_NULL();
	}

	config = zend_read_property(wk_config_ce, getThis(), ZEND_STRL("config"), 0 TSRMLS_CC);
	zval_addref_p(config);

	if(zend_hash_find(Z_ARRVAL_P(config), name, name_len+1, (void**)&ret) == SUCCESS){
		RETURN_ZVAL(*ret, 1, 0);
	}else{
		RETURN_NULL();
	}

}

static zend_function_entry wk_config_method[] = {
	ZEND_ME(wk_config, __construct, void_arginfo, ZEND_ACC_CTOR|ZEND_ACC_PUBLIC)
	ZEND_ME(wk_config, item, item_arginfo, ZEND_ACC_CTOR|ZEND_ACC_PUBLIC)
	{NULL, NULL, NULL}
};

EXT_STARTUP_FUNCTION(wk_config){
	zend_class_entry wk_config;
	INIT_CLASS_ENTRY(wk_config, "wk_config", wk_config_method);
	wk_config_ce = zend_register_internal_class(&wk_config TSRMLS_CC);
	zend_declare_property_null(wk_config_ce, ZEND_STRL("config"), ZEND_ACC_PUBLIC TSRMLS_CC);
	return SUCCESS;
}